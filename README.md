# apex mocks

[![GoReportCard](https://goreportcard.com/badge/gitlab.com/unboundsoftware/apex-mocks)](https://goreportcard.com/report/gitlab.com/unboundsoftware/apex-mocks) [![GoDoc](https://godoc.org/gitlab.com/unboundsoftware/apex-mocks?status.svg)](https://godoc.org/gitlab.com/unboundsoftware/apex-mocks) [![Build Status](https://gitlab.com/unboundsoftware/apex-mocks/badges/master/pipeline.svg)](https://gitlab.com/unboundsoftware/apex-mocks/commits/master)[![coverage report](https://gitlab.com/unboundsoftware/apex-mocks/badges/master/coverage.svg)](https://gitlab.com/unboundsoftware/apex-mocks/commits/master)

Package `mocks` provides a mock implementation of [Apex](https://github.com/apex/logs) which can be used to unit test applications using Apex logging.

### Download
```shell
go get gitlab.com/unboundsoftware/apex-mocks
```

### Usage
Make sure that your application uses `log.Interface` to be able to pass in a `Mock` from your tests instead.

To verify what has been logged:
```go
  logger := &apex.Mock{}
  // ... run your actual test using the logger above
  logger.Check(t, tt.wantLogged)
```
