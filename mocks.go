// Copyright (c) 2022 Unbound Software Development Svenska AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package apex

import (
	"fmt"
	"sync"
	"testing"

	"github.com/apex/log"
	"github.com/stretchr/testify/assert"
)

// Mock has a Logger for use in unit-testing
type Mock struct {
	*sync.RWMutex
	log.Interface
	Logged []string
}

// HandleLog stores the logged entries to be able to check them later
func (m *Mock) HandleLog(entry *log.Entry) error {
	m.Lock()
	defer m.Unlock()
	m.Logged = append(m.Logged, fmt.Sprintf("%s: %s", entry.Level.String(), entry.Message))
	return nil
}

var _ log.Handler = &Mock{}

// New instantiates a new Mock and sets the log.Handler to it
func New() *Mock {
	mock := &Mock{
		RWMutex:   &sync.RWMutex{},
		Interface: log.Log,
		Logged:    make([]string, 0),
	}
	log.SetHandler(mock)
	return mock
}

// Check verifies that the application has logged the expected strings
func (m *Mock) Check(t testing.TB, wantLogged []string) {
	t.Helper()
	m.RLock()
	defer m.RUnlock()
	if wantLogged == nil {
		wantLogged = []string{}
	}
	assert.Equal(t, wantLogged, m.Logged)
}
