// Copyright (c) 2022 Unbound Software Development Svenska AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package apex

import (
	"fmt"
	"regexp"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMock_Check(t *testing.T) {
	type fields struct {
		Logged []string
	}
	type args struct {
		wantLogged []string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []string
	}{
		{
			name:   "same",
			fields: fields{Logged: []string{"same"}},
			args:   args{wantLogged: []string{"same"}},
			want:   nil,
		},
		{
			name:   "different",
			fields: fields{Logged: []string{"same"}},
			args:   args{wantLogged: []string{"different"}},
			want:   []string{"\n\tError:      \tNot equal: \n\t            \texpected: []string{\"different\"}\n\t            \tactual  : []string{\"same\"}\n\t            \t\n\t            \tDiff:\n\t            \t--- Expected\n\t            \t+++ Actual\n\t            \t@@ -1,3 +1,3 @@\n\t            \t ([]string) (len=1) {\n\t            \t- (string) (len=9) \"different\"\n\t            \t+ (string) (len=4) \"same\"\n\t            \t }\n\tTest:       \t\n"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Mock{
				RWMutex: &sync.RWMutex{},
				Logged:  tt.fields.Logged,
			}
			temp := &MockT{}
			m.Check(temp, tt.args.wantLogged)
			assert.Equal(t, tt.want, temp.errors)
		})
	}
}

func TestNew(t *testing.T) {
	logger := New()
	logger.Info("logging")
	logger.Check(t, []string{"info: logging"})
}

type MockT struct {
	testing.T
	errors []string
}

func (m *MockT) Errorf(format string, args ...interface{}) {
	re, err := regexp.Compile(`(?s)^\tError Trace:.*(\tError:.*)$`)
	assert.NoError(m, err)
	for i, a := range args {
		if s, ok := a.(string); ok {
			args[i] = re.ReplaceAllString(s, "$1")
		}
	}
	m.errors = append(m.errors, fmt.Sprintf(format, args...))
}
